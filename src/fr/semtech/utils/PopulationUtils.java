package fr.semtech.utils;


import fr.semtech.domain.Commune;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PopulationUtils {

    private static final Logger logger = Logger.getLogger(PopulationUtils.class.getName());

    public static final String CSV_DELIMITER_CHAR = ";";
    public static final String EMPTY_STRING = "";

    public static final int DEPARTMENT_CODE_COLUMN_NUMBER = 0;
    public static final int DEPARTMENT_NAME_COLUMN_NUMBER = 3;
    public static final int COMMUNE_NAME_COLUMN_NUMBER = 1;
    public static final int COMMUNE_POPULATION_COLUMN_NUMBER = 2;

    public static int calculateCommuneTotalPopulation(List<Commune> communes) {
        return communes.stream().map(Commune::getPopulation).reduce(0, Integer::sum);
    }

    public static String getFieldValue(String[] fields, int index) {
        try {
            return fields[index];
        } catch (ArrayIndexOutOfBoundsException e) {
            logger.log(Level.WARNING, "Error getting field value at index " + index, e.getMessage());
            return EMPTY_STRING;
        }
    }

    public static Commune createCommune(String[] fields) {
        Commune commune = new Commune();
        commune.setName(fields[COMMUNE_NAME_COLUMN_NUMBER]);
        commune.setPopulation(Integer.parseInt(fields[COMMUNE_POPULATION_COLUMN_NUMBER]));
        return commune;
    }

    public static void displayDepartmentPopulations(Map<String, List<Commune>> communesByCode, Map<String, String> departments) {
        AtomicReference<String> departmentCodeWithSmallestPopulation = new AtomicReference<>(EMPTY_STRING);
        AtomicReference<Integer> smallestPopulation = new AtomicReference<>(Integer.MAX_VALUE);

        communesByCode.forEach((code, communes) -> {
            int communeTotalPopulation = PopulationUtils.calculateCommuneTotalPopulation(communes);
            if (communeTotalPopulation < smallestPopulation.get()) {
                smallestPopulation.set(communeTotalPopulation);
                departmentCodeWithSmallestPopulation.set(code);
            }
            System.out.println("Department " + code + " " + departments.get(code) + " have a total " +
                    "population of " + communeTotalPopulation);
        });

        System.out.println("Department" + " " + departmentCodeWithSmallestPopulation.get() + " "
                + departments.get(departmentCodeWithSmallestPopulation.get()) + " have smallest population "
                + smallestPopulation.get());
    }

    public static void processCsv(BufferedReader bufferedReader, Map<String, List<Commune>> communesByCode, Map<String,
            String> departments) throws IOException {
        String line;
        bufferedReader.readLine();
        while ((line = bufferedReader.readLine()) != null) {
            String[] fields = line.split(CSV_DELIMITER_CHAR);
            if (fields.length > 0) {
                String departmentCode = PopulationUtils.getFieldValue(fields, DEPARTMENT_CODE_COLUMN_NUMBER);
                String departmentName = PopulationUtils.getFieldValue(fields, DEPARTMENT_NAME_COLUMN_NUMBER);
                if (!departmentName.isEmpty()) {
                    departments.put(departmentCode, departmentName);
                }
                Commune commune = PopulationUtils.createCommune(fields);
                communesByCode.computeIfAbsent(departmentCode, k -> new ArrayList<>()).add(commune);
            }
        }
    }
}
