package fr.semtech.domain;

import java.util.Objects;

public class Commune {
    private String name;
    private int population;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Commune commune)) return false;
        return getPopulation() == commune.getPopulation() && Objects.equals(getName(), commune.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPopulation());
    }

    @Override
    public String toString() {
        return "Commune{" +
                "name='" + name + '\'' +
                ", population=" + population +
                '}';
    }
}
