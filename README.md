# population-app
### Java application for calculation french communes population per department


## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Deployment](#deployment)

## Installation

### Build Application

To build the application, run the following commands:

```sh
javac -d out -sourcepath src src/fr/semtech/PopulationApp.java
jar cfm PopulationApp.jar MANIFEST.MF -C out .
```

## Usage

### Run Application

To run the application, use the following command, replacing `${CSV_URL}` with the URL to your CSV file:
```sh
java -jar PopulationApp.jar ${CSV_URL}
```

## Deployment

Deployment is handled using GitLab CI/CD with the following stages defined in the `.gitlab-ci.yml` file:

1. **build-jar**: Generates a JAR archive containing application classes.
2. **deploy**: Deploys the code to an AWS Lambda function called `population-semtech-test` using `aws cli`.

### GitlabCI `.gitlab-ci.yml`

```yaml
stages:
  - build-jar
  - deploy

before_script:
  - export "PATH=/usr/lib/jvm/java-17-openjdk/bin:$PATH"

build-jar:
  stage: build-jar
  image: openjdk:17
  script:
    - mkdir -p out
    - javac -d out -sourcepath src src/fr/semtech/PopulationApp.java
    - jar cfm PopulationApp.jar MANIFEST.MF -C out .
  artifacts:
    paths:
      - PopulationApp.jar
deploy:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  script:
    - aws lambda update-function-code --function-name population-semtech-test --zip-file fileb://PopulationApp.jar
  only:
    - main
