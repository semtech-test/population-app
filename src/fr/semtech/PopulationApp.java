package fr.semtech;


import fr.semtech.domain.Commune;
import fr.semtech.exception.PopulationException;
import fr.semtech.utils.PopulationUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class PopulationApp {

    private static final Logger logger = Logger.getLogger(PopulationApp.class.getName());

    public static void main(String[] args) {
        if (args.length < 1) {
            logger.log(Level.SEVERE, "You need to pass the CSV url as first argument");
        } else {
            String csvUrl = args[0];
            if (csvUrl == null || csvUrl.isEmpty()) {
                logger.log(Level.SEVERE, "CSV URL is null or empty");
                throw new PopulationException("CSV URL is null or empty");
            }

            try {
                URL url = new URL(csvUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");

                Map<String, List<Commune>> communesByCode = new HashMap<>();
                Map<String, String> departments = new HashMap<>();

                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                    PopulationUtils.processCsv(bufferedReader, communesByCode, departments);
                } finally {
                    connection.disconnect();
                }

                PopulationUtils.displayDepartmentPopulations(communesByCode, departments);
            } catch (IOException e) {
                logger.log(Level.WARNING, "Error during reading a CSV file from URL: " + csvUrl, e);
                throw new PopulationException("Error during reading a CSV file from URL: " + csvUrl);
            }
        }
    }
}
